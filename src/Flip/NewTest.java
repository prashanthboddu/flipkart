package Flip;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewTest {
	public static WebDriver driver;
	 
	 @BeforeTest
	 public void loginToApp(WebDriver driver) throws InterruptedException
	 {
	//System.setProperty("webdriver.gecko.driver","C:\\Users\\prashanthb\\workspace\\Ali Express\\drivers\\geckodriver.exe");
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\prashanthb\\workspace\\Flipkart\\Drivers\\chromedriver.exe");
	 
	ChromeOptions options = new ChromeOptions();
	  options.addArguments("--disable-notifications"); // to remove browser pop-ups
	  driver = new ChromeDriver(options);
	  //WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
  String baseUrl = "https://www.flipkart.com/";
  // launch browser and direct it to the Base URL

  driver.get(baseUrl);

  System.out.println(driver.getTitle()); //to get the title of the page

  Thread.sleep(3000);

  driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[1]/input")).sendKeys("prashanthnani201993@gmail.com");//Enter email id

  driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[2]/input")).sendKeys("nani201993");// Enter password

  driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[3]/button/span")).click();//Login button
	 }
	 @Test(priority=0)
	 public void navigateToflow(WebDriver driver) throws InterruptedException
	 {
  Thread.sleep(3000);

  driver.findElement(By.xpath("//*[@id='container']/div/header/div[1]/div/div/div/div[1]/form/div/div[1]/div/input")).sendKeys("samsung edge"); // Search bar

  driver.findElement(By.xpath("//button[@type='submit']")).click();//Search icon  button

  Thread.sleep(3000);

  driver.findElement(By.xpath("//*[@id='container']/div/div[1]/div/div[2]/div/div[2]/div/div[3]/div/div/div[1]/div/a/div[3]/div[1]/div[1]")).click();// Select a mobile device

  ArrayList tabs = new ArrayList (driver.getWindowHandles());//Switch to other tab
  System.out.println(tabs.size());//Print number of tabs open on the browser

  // switch between windows

  driver.switchTo().window((String) tabs.get(1));//Redirect to 1st tab
  Thread.sleep(3000);
  driver.findElement(By.xpath("//*[@id='container']/div/div[1]/div/div/div/div[1]/div/div[1]/div[2]/ul/li[1]/button")).click();// click Add to cart button

  driver.findElement(By.xpath("//img[@src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/fk-logo_9fddff.png']")).click();// redirect to Homepage

  Thread.sleep(3000);

  driver.findElement(By.xpath("//*[@id='container']/div/header/div[1]/div/div/div/div[1]/form/div/div[1]/div/input")).sendKeys("iphone x");// Search iPhone

  driver.findElement(By.xpath("//button[@type='submit']")).click();//Search icon button

  Thread.sleep(3000);

  driver.findElement(By.xpath("//*[@id='container']/div/div[1]/div/div[2]/div/div[2]/div/div[3]/div/div/div[1]/div/a/div[3]/div[1]/div[1]")).click();//select a mobile device


  Thread.sleep(3000);
  ArrayList tab1 = new ArrayList (driver.getWindowHandles());//Switch to other tab

  System.out.println(tab1.size());//Print number of tabs open on the browser

  // switch between windows

  driver.switchTo().window((String) tab1.get(2));//Redirect to second tab
  driver.findElement(By.xpath("//button[@class='_2AkmmA _2Npkh4 _2MWPVK']")).click();//Add to cart button
  Thread.sleep(3000);
  driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div/div/div[1]/div[3]/div/div/form/button/span")).click();//Place order button
  Thread.sleep(3000);
  driver.findElement(By.xpath("//*[@id='CNTCT4C40C570E8F74FB5B9D521787']/button")).click();///Deliver here button
  driver.findElement(By.xpath("//button[@class='_2AkmmA _2Q4i61 _7UHT_c']")).click();//continue button
  Thread.sleep(3000);
  driver.findElement(By.xpath("//div[@class='_1GRhLX _17_fE5']//label/input[@id='PHONEPE']/../div")).click();
  driver.findElement(By.xpath("//img[@src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/fk-logo_9fddff.png']")).click();// redirect to Homepage
  Thread.sleep(3000);
  driver.findElement(By.xpath("//span[@class='DJrnBo']")).click();//Cart button
  Thread.sleep(3000);
  for(int i=0;i<2;i++){
  driver.findElement(By.xpath("//span[text()='Remove']")).click();//remove an item from cart
Thread.sleep(3000);}
 
  driver.findElement(By.xpath("//span[@class='_2cyQi_']")).click();//remove an item from cart
	 }
	 @AfterTest
	 public void logout(WebDriver driver) throws InterruptedException
	 {
  driver.findElement(By.xpath("//a[@href='/account/?rd=0&link=home_account']")).click();//My account tab
  Thread.sleep(3000);
  driver.findElement(By.linkText("Logout")).click();//Logout
  Thread.sleep(5000);
  driver.quit();
  }
}
